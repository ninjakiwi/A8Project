package com.team.msppitc.a8project.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.team.msppitc.a8project.game.gameobject.Circle;
import com.team.msppitc.a8project.game.gameobject.GameObject;
import com.team.msppitc.a8project.game.gameobject.Goal;
import com.team.msppitc.a8project.game.gameobject.Player;
import com.team.msppitc.a8project.game.gameobject.PointsCircle;
import com.team.msppitc.a8project.game.gameobject.Rectangle;
import com.team.msppitc.a8project.game.gameobject.Rigidbody;
import com.team.msppitc.a8project.game.gameobject.StaticObject;
import com.team.msppitc.a8project.game.helpers.Collision;
import com.team.msppitc.a8project.game.helpers.GameListener;
import com.team.msppitc.a8project.game.helpers.ParticleSystem;
import com.team.msppitc.a8project.game.helpers.RailsConstraint;
import com.team.msppitc.a8project.game.helpers.Vector2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class GameView extends SurfaceView implements Runnable, SensorEventListener {
    class MyGestureListener implements GestureDetector.OnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            if (flicked) {
                return false;
            }

            Vector2 push = new Vector2(velocityX, velocityY).mult(1 / gameScaleRatio);
            Vector2 pushAdj = push.normalize().mult(Math.min(push.magnitude(), 10000));

            player.addForce(pushAdj.mult(60));
            flicked = true;
            return false;
        }
    }


    private SurfaceHolder mSurfaceHolder;
    private GameListener gl;
    private int mViewWidth;
    private int mViewHeight;
    private static final int inGameWidth = 900;
    private static final int inGameHeight = 1600;
    private double gameScaleRatio = 0;
    private static final int FPS = 60;
    private Vector2 gameOffset = new Vector2();
    private boolean mRunning;
    private Thread mGameThread;
    private LinkedList<Rigidbody> rigidBodies;
    private LinkedList<ParticleSystem> particleSystems;
    private LinkedList<Rigidbody> nonColliderRigidbodies;
    private LinkedList<GameObject> decorations;
    private Player player;
    public int score;

    private double lastTime;

    private Vector2 gravity;

    private static final int backgroundColour = Color.BLACK;
    private static final int playerColour = Color.WHITE;
    private static final int wallColour = Color.DKGRAY;
    private static final int objectColour = Color.GRAY;
    private static final int goalColour = Color.RED;


    private Paint backgroundPaint;

    private static final double playerGravityFactor = 200;
    private static final double tiltGravityFactor = 200;

    private static final int wallSize = 64;

    private GestureDetectorCompat gestureDetector;

    private boolean flicked = false;

    private void init(Context context) {
        mSurfaceHolder = getHolder();

        backgroundPaint = new Paint();
        backgroundPaint.setColor(backgroundColour);

        score = 0;

        rigidBodies = new LinkedList<>();
        decorations = new LinkedList<>();
        particleSystems = new LinkedList<>();
        nonColliderRigidbodies = new LinkedList<>();

        gravity = new Vector2();

        SensorManager senSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        assert senSensorManager != null; // This practically crashes the program anyway but is apparently good practice
        Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_GAME);

        MyGestureListener gestureListener = new MyGestureListener();
        gestureDetector = new GestureDetectorCompat(context, gestureListener);

        lastTime = System.nanoTime() / 1000000000.0;
    }

    public void setGameListener(GameListener gameListener) {
        gl = gameListener;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mViewWidth = w;
        mViewHeight = h;
        setupMap();
    }

    public void endGame() {
        gl.endGame(score);
    }

    private void setupMap() {
        if ((mViewWidth / (float) mViewHeight) < (inGameWidth / (float) inGameHeight)) {
            gameScaleRatio = mViewWidth / (float) inGameWidth;
            gameOffset.x = 0;
            gameOffset.y = (mViewHeight - inGameHeight * gameScaleRatio) / 2;
        } else {
            gameScaleRatio = mViewHeight / (float) inGameHeight;
            gameOffset.x = (mViewWidth - inGameWidth * gameScaleRatio) / 2;
            gameOffset.y = 0;
        }

        player = new Player(new Vector2(inGameWidth / 2, 3 * inGameHeight / 4), 0, 35, playerColour, 10);
        rigidBodies.add(player); // Added by reference

        {
            Circle item = new Circle(new Vector2(300, 150), 0, 30, objectColour, 10);
            rigidBodies.add(item);
        }

        {
            Circle item = new Circle(new Vector2(inGameWidth - 300, 150), 0, 30, objectColour, 10);
            rigidBodies.add(item);
        }


        // on rails
        {
            Rectangle item = new Rectangle(new Vector2(300, 400), Math.PI / 3, 220, 100, objectColour, 40,
                    new RailsConstraint(new Vector2(300, 400), true, 100, new Vector2(1, 0).normalize()));
            rigidBodies.add(item);
        }
        {
            Rectangle item = new Rectangle(new Vector2(inGameWidth - 300, 400), -Math.PI / 3, 220, 100, objectColour, 40,
                    new RailsConstraint(new Vector2(inGameWidth - 300, 400), true, 100, new Vector2(1, 0).normalize()));
            rigidBodies.add(item);
        }
        {
            PointsCircle item = new PointsCircle(this, new Vector2(inGameWidth / 2, 850), 0, 50, Color.GREEN, 35,
                    new RailsConstraint(new Vector2(inGameWidth / 2, 850), false, 150, new Vector2(0, 1).normalize()));
            rigidBodies.add(item);
        }

        // Bottom Rails
        {
            int width = 150;
            Vector2 pos = new Vector2(inGameWidth / 4, inGameHeight - 400);
            Rectangle item = new Rectangle(pos, Math.PI / 4, width, 50, objectColour, 30, new RailsConstraint(new Vector2(pos), false, inGameWidth / 4 - width / 2, new Vector2(1, 1)));
            item.restitution = 5;
            rigidBodies.add(item);
        }
        {
            int width = 150;
            Vector2 pos = new Vector2(inGameWidth - inGameWidth / 4, inGameHeight - 400);
            Rectangle item = new Rectangle(pos, -Math.PI / 4, width, 50, objectColour, 30, new RailsConstraint(new Vector2(pos), false, inGameWidth / 4 - width / 2, new Vector2(-1, 1)));
            item.restitution = 5;
            rigidBodies.add(item);
        }

        // Particles
        {
            ParticleSystem item = new ParticleSystem(new Vector2(wallSize / 2, inGameHeight - wallSize / 4), new Vector2(100, -200).normalize(), 400, 0.15, Color.rgb(255, 128, 0));
            particleSystems.add(item);
        }
        {
            ParticleSystem item = new ParticleSystem(new Vector2(inGameWidth - wallSize / 2, inGameHeight - wallSize / 4), new Vector2(-100, -200).normalize(), 400, 0.15, Color.rgb(255, 128, 0));
            particleSystems.add(item);
        }

        // Goal
        Goal goal = new Goal(new Vector2(inGameWidth / 2, inGameHeight), 0, inGameWidth, wallSize, goalColour, this);
        rigidBodies.add(goal);

        {
            StaticObject item = new StaticObject(new Vector2(wallSize / 2, inGameHeight / 4), Math.PI / 4, wallSize, wallSize, wallColour);
            rigidBodies.add(item);
        }
        {
            StaticObject item = new StaticObject(new Vector2(inGameWidth - wallSize / 2, inGameHeight / 4), Math.PI / 4, wallSize, wallSize, wallColour);
            rigidBodies.add(item);
        }
        {
            StaticObject item = new StaticObject(new Vector2(wallSize / 2, inGameHeight / 2), Math.PI / 4, wallSize, wallSize, wallColour);
            rigidBodies.add(item);
        }
        {
            StaticObject item = new StaticObject(new Vector2(inGameWidth - wallSize / 2, inGameHeight / 2), Math.PI / 4, wallSize, wallSize, wallColour);
            rigidBodies.add(item);
        }
        StaticObject topStaticObject = new StaticObject(new Vector2(inGameWidth / 2, 0), 0, inGameWidth, wallSize, wallColour);
        StaticObject leftStaticObject = new StaticObject(new Vector2(0, inGameHeight / 2), 0, wallSize, inGameHeight, wallColour);
        StaticObject rightStaticObject = new StaticObject(new Vector2(inGameWidth, inGameHeight / 2), 0, wallSize, inGameHeight, wallColour);
        rigidBodies.add(topStaticObject);
        rigidBodies.add(leftStaticObject);
        rigidBodies.add(rightStaticObject);

    }

    public GameView(Context context) {
        super(context);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    @Override
    public void run() {
        Canvas canvas;
        while (mRunning) {
            if (!mSurfaceHolder.getSurface().isValid()) {
                continue;
            }

            double deltaTime = System.nanoTime() / 1000000000.0 - lastTime;

            double targetDeltaTime = (1 / (float) FPS);

            if (deltaTime < targetDeltaTime) {
                continue;
            }

//            int numOfBodies = rigidBodies.size();

            if (flicked) {
                score += 1;
                gl.updateScore(score);

                player.addForce(new Vector2(0, 1).mult(playerGravityFactor * player.getMass())); // Real gravity
            }

            // Update loop:
            for (Rigidbody thingy : rigidBodies) {
                if (flicked && !(thingy instanceof Player)) {
                    thingy.addForce(gravity.mult(tiltGravityFactor * thingy.getMass()));
                }
                thingy.Update(targetDeltaTime);
            }

            // Collision loops
            ListIterator<Rigidbody> outerIter = rigidBodies.listIterator();
            while (outerIter.hasNext()) {
                Rigidbody rigidbody1 = outerIter.next();
                {
                    Vector2 pos = rigidbody1.getPosition();
                    Vector2 adjusted = new Vector2(Math.max(wallSize / 2.0 + 1, Math.min(pos.x, inGameWidth - wallSize / 2.0 - 1)), Math.max(wallSize / 2.0 + 1, Math.min(pos.y, inGameHeight - wallSize / 2.0 - 1)));
                    if (!(rigidbody1 instanceof StaticObject)) {
                        rigidbody1.setPosition(new Vector2(adjusted));
                    }
                }

                // THIS LINE HAS TO LOOP THROUGH EVERY ITEM BEFORE IT TO GET BACK TO WHERE WE ARE ALREADY!!! WHY ON EARTH DOES JAVA NOT HAVE A WAY TO COPY ITERATORS.
                ListIterator<Rigidbody> innerIter = rigidBodies.listIterator(outerIter.nextIndex());
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                while (innerIter.hasNext()) {
                    Rigidbody rigidbody2 = innerIter.next();
                    Vector2[] hitPos = ((Collision) rigidbody1).intersect((Collision) rigidbody2);

                    if (hitPos != null) {
                        doCollision(rigidbody1, rigidbody2, hitPos[0], hitPos[1]);
                    }
                }
            }

            RemoveDeadObjects(rigidBodies);
            RemoveDeadObjects(nonColliderRigidbodies);

            for (ParticleSystem system : particleSystems) {
                system.Update(targetDeltaTime, nonColliderRigidbodies);
            }

            // Draw Objects to canvas here
            canvas = mSurfaceHolder.lockCanvas();
            canvas.save();
            canvas.drawColor(wallColour);
            canvas.translate((float) gameOffset.x, (float) gameOffset.y);
            canvas.drawRect(0, 0, (float) (inGameWidth * gameScaleRatio), (float) (inGameHeight * gameScaleRatio), backgroundPaint);

            for (Rigidbody thingy : rigidBodies) {
                thingy.constraintManagerDrawRail(canvas, gameScaleRatio);
            }
            for (Rigidbody thingy : rigidBodies) {
                thingy.Draw(canvas, gameScaleRatio);
            }
            for (GameObject thingy : decorations) {
                thingy.Draw(canvas, gameScaleRatio);
            }

            for (Rigidbody thingy : nonColliderRigidbodies) {
                thingy.Update(deltaTime);
                thingy.addForce(gravity.mult(tiltGravityFactor * 0.5 * thingy.getMass()));
                thingy.Draw(canvas, gameScaleRatio);
            }

            canvas.restore();
            mSurfaceHolder.unlockCanvasAndPost(canvas);
            lastTime += deltaTime;
        }
    }

    private void doCollision(Rigidbody body1, Rigidbody body2, Vector2 position, Vector2 delta) {
        body1.move(delta);

        // Don't try to optimize this, this is done this way to reu se the variables for moving the player and other.

        double forcePlayer;
        double forceOther;
        double forceDiff;
        Vector2 playerVelProjected;
        Vector2 otherVelProjected;
        playerVelProjected = body1.getVelocity(position).projection(delta);
        otherVelProjected = body2.getVelocity(position).projection(delta);
        forceDiff = playerVelProjected.mult(body1.getMass()).add(otherVelProjected.mult(-body2.getMass())).magnitude();
        forcePlayer = playerVelProjected.magnitude() * -Math.signum(playerVelProjected.dot(delta));
        forceOther = otherVelProjected.magnitude() * -Math.signum(otherVelProjected.dot(delta));


        Vector2 deltaDir = delta.normalize();
        double forceAmount = forceDiff * FPS * (0.5 + (body2.getRestitution() + body1.getRestitution()) / 4);

        body1.addForce(deltaDir.mult(-forcePlayer + forceAmount), position);

        body2.addForce(deltaDir.mult(-forceOther - forceAmount), position);
    }

    public void pause() {
        mRunning = false;
        try {
            // Stop the thread (rejoin the main thread)
            mGameThread.join();
        } catch (InterruptedException e) {
            Log.e("202", "GAME THREAD DIED");
        }
    }

    public void resume() {
        lastTime = System.nanoTime() / 1000000000.0;
        mRunning = true;
        mGameThread = new Thread(this);
        mGameThread.start();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        gravity.x = -sensorEvent.values[0];
        gravity.y = sensorEvent.values[1];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return true;
    }

    public LinkedList<Rigidbody> getNonColliderRigidbodies() {
        return nonColliderRigidbodies;
    }

    private static void RemoveDeadObjects(LinkedList<Rigidbody> list) {
        ListIterator<Rigidbody> iter = list.listIterator();
        while (iter.hasNext()) {
            if (iter.next().isDead()) {
                iter.remove();
            }
        }
    }
}