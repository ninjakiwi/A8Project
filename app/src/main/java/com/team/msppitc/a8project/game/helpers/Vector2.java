package com.team.msppitc.a8project.game.helpers;


public class Vector2 {
    public double x, y;

    public Vector2() {
        x = 0; y = 0;
    }
    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public Vector2(Vector2 a) {
        x = a.x;
        y = a.y;
    }

    public Vector2 normalize() {
        return this.mult(1 / magnitude());
    }

    public Vector2 add(Vector2 other) {
        return new Vector2(this.x + other.x, this.y + other.y);
    }

    public Vector2 sub(Vector2 other) {
        return new Vector2(this.x - other.x, this.y - other.y);
    }

    public Vector2 mult(double scalar) {
        return new Vector2(this.x * scalar, this.y * scalar);
    }

    public void multMut(double scalar) {
        this.x *= scalar;
    }

    public void addMut(Vector2 other) {
        this.x += other.x;
        this.y += other.y;
    }

    public double magnitude() {
        return Math.sqrt(magnitudeSqr());
    }

    public double magnitudeSqr() {
        return x * x + y * y;
    }

    public void reset() {
        x = 0;
        y = 0;
    }

    public Vector2 tangent() {
        //noinspection SuspiciousNameCombination
        return new Vector2(-y, x);
    }

    public double dot(Vector2 other) {
        return this.x * other.x + this.y * other.y;
    }

    /**
     * Returns the vector of this vector projected onto the onto vector
     * @param onto The vector to project onto
     */
    public Vector2 projection(Vector2 onto) {
        Vector2 ontoNorm = onto.normalize();
        double scalar = this.dot(ontoNorm);
        return ontoNorm.mult(scalar);
    }

    public Vector2 rotate(double radians) {
        return rotateAround(radians, new Vector2());
    }

    public Vector2 rotateAround(double radians, Vector2 origin)
    {
        Vector2 vecAroundOrigin = this.add(origin.mult(-1));
        double ca = Math.cos(radians);
        double sa = Math.sin(radians);
        return new Vector2(ca*vecAroundOrigin.x - sa*vecAroundOrigin.y, sa*vecAroundOrigin.x + ca*vecAroundOrigin.y).add(origin);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Vector2)) {
            return false;
        }

        final Vector2 other = (Vector2) obj;
        return doubleEquals(other.x, this.x) && doubleEquals(other.y, this.y);
    }

    private final static double EPS = 0.0000001;
    public static boolean  doubleEquals(double a, double b) {
        return a == b || Math.abs(a - b) < EPS * Math.max(Math.abs(a), Math.abs(b));
    }

    @Override
    public String toString() {
        return "{x: " + x + " , y: " + y + "}";
    }
}
