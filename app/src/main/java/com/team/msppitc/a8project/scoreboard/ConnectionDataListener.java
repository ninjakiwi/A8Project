package com.team.msppitc.a8project.scoreboard;

import java.util.ArrayList;

public interface ConnectionDataListener {
    void dataReturned(ArrayList<String> allNames, ArrayList<Integer> allScores);
}
