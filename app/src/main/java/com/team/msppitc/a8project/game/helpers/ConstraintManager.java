package com.team.msppitc.a8project.game.helpers;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.team.msppitc.a8project.game.gameobject.Rigidbody;

public abstract class ConstraintManager {
    protected Vector2 startingPos;
    public ConstraintManager(Vector2 startingPos) {
        this.startingPos = startingPos;
    }
    public abstract void constraintVelocity(double deltaTime, Rigidbody rb);
    public abstract void constraintPosition(double deltaTime, Rigidbody rb);

    public abstract void drawRails(Canvas canvas, double scale, Paint paint);
}
