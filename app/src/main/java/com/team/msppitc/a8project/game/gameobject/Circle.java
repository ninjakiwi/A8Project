package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.team.msppitc.a8project.game.helpers.Colliders;
import com.team.msppitc.a8project.game.helpers.Collision;
import com.team.msppitc.a8project.game.helpers.ConstraintManager;
import com.team.msppitc.a8project.game.helpers.Vector2;

public class Circle extends Rigidbody implements Collision {

    private double boundingRadius;

    public Circle(Vector2 pos, double angle, int radius, int intendedColor, double mass) {
        super(pos, angle, radius*2, radius*2, intendedColor, mass);
        restitution = 1.7;
        init();
    }
    public Circle(Vector2 pos, double angle, int radius, int intendedColor, double mass, ConstraintManager cm) {
        super(pos, angle, radius*2, radius*2, intendedColor, mass, cm);
        init();
    }

    public void init() {
        boundingRadius = getRadius();
    }

    public double getRadius() {
        return width/2.0;
    }

    @Override
    public Vector2[] intersect(Collision collision) {
        if (position.sub(collision.getPosition()).magnitudeSqr() < Math.pow(collision.getBoundingRadius() + getBoundingRadius(),2)) {
            return collision.intersectCircle(this);
        } else {
            return null;
        }
    }

    @Override
    public Vector2[] intersectCircle(Circle c) {
        // Thing is a circle
        Vector2 c2Pos = c.getPosition();
        double c1Radius = getRadius();
        double c2Radius = c.getRadius();

        Vector2 posDelta = c2Pos.sub(getPosition());
        double diff = posDelta.magnitude() - (c1Radius + c2Radius);
        if (diff >= 0) {
            return null;
        }
        Vector2 posDeltaNorm = posDelta.normalize();
        Vector2 radiusVec2 = posDeltaNorm.mult(c2Radius);

        return new Vector2[] {c2Pos.sub(radiusVec2), posDeltaNorm.mult(-diff)};
    }

    @Override
    public Vector2[] intersectRectangle(Rectangle r) {
        // This might not work
        // Thing is a rectangle
        Vector2 circlePos = getPosition();
        double circleRadius = getRadius();
        Vector2[] recCorners = r.getCorners();
        double recAngle = r.getAngle();

        for (int i = 0; i < recCorners.length; i++) {
            // Check edge
            Vector2 edge = Colliders.intersectCircleOnLine(circlePos, circleRadius, recCorners[i], recCorners[(i+1) % recCorners.length], recAngle);
            if (edge != null) {
                Vector2 circleToCorner = edge.sub(circlePos);
                Vector2 directionOfImpact = circleToCorner.normalize();
                Vector2 pointOfFurthermostDecent = directionOfImpact.mult(getRadius()); // from circle
                return new Vector2[] {edge, circlePos.sub(pointOfFurthermostDecent)};
            }

            // Check corner
            if (circlePos.sub(recCorners[i]).magnitude() < circleRadius) {
                Vector2 circleToCorner = recCorners[i].sub(circlePos);
                Vector2 directionOfImpact = circleToCorner.normalize();
                Vector2 pointOfFurthermostDecent = directionOfImpact.mult(getRadius()); // from circle
                return new Vector2[] {recCorners[i], circlePos.sub(pointOfFurthermostDecent)};
            }
        }
        return null;
    }

    @Override
    public double getBoundingRadius() {
        return boundingRadius;
    }

    @Override
    public void Draw(Canvas canvas, double ratio) {

        Paint[] allPaints = MakeItGlow(intendedColor, (float)(getRadius() * 2.5 * ratio), position.mult(ratio));
        for (int i = 1; i < allPaints.length; i++) {
            canvas.drawCircle((float) (position.x * ratio), (float) (position.y * ratio), (float)(getRadius() * ratio * 2.5), allPaints[i]);
        }
        canvas.drawCircle((float) (position.x * ratio), (float) (position.y * ratio), (float)(getRadius() * ratio), allPaints[0]);

        constraintManagerDrawCenter(canvas, ratio);
    }
}
