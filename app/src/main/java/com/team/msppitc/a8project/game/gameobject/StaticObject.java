package com.team.msppitc.a8project.game.gameobject;

import com.team.msppitc.a8project.game.helpers.Collision;
import com.team.msppitc.a8project.game.helpers.Vector2;

public class StaticObject extends Rectangle {
    public StaticObject(Vector2 pos, double angle, int width, int height, int intendedColor) {
        super(pos, angle, width, height, intendedColor, Math.pow(10, 50), null);
        restitution = 1;
    }

    @Override
    public Vector2 getVelocity(Vector2 position) {
        return new Vector2();
    }

    @Override
    public void addForce(Vector2 force) {
    }

    @Override
    public void addForce(Vector2 force, Vector2 position) {
    }

    @Override
    public Vector2[] intersect(Collision collision) {
        return null;
    }
}
