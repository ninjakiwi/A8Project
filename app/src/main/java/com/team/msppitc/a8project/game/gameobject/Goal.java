package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;

import com.team.msppitc.a8project.game.GameView;
import com.team.msppitc.a8project.game.helpers.Vector2;

import java.util.Random;

public class Goal extends StaticObject {

    Random r;
    private int baseColorIntensity = 96;
    private GameView gameView;

    public Goal(Vector2 pos, double angle, int width, int height, int intendedColor, GameView gv) {
        super(pos, angle, width, height, intendedColor);
        r = new Random();
        gameView = gv;
    }

    @Override
    public Vector2[] intersectCircle(Circle c) {
        Vector2[] result = super.intersectCircle(c);

        if (result != null) {
            baseColorIntensity = 180;
            if (c instanceof Player) {
                scoreGoal();
            }
        }
        return result;
    }

    @Override
    public Vector2[] intersectRectangle(Rectangle r) {
        Vector2[] result =  super.intersectRectangle(r);
        if (result != null) {
            baseColorIntensity = 180;
        }
        return result;
    }

    private void scoreGoal() {
        gameView.endGame();
    }

    @Override
    public void Draw(Canvas canvas, double ratio) {

        Vector2[] corners = getCorners();

        Path path = new Path();
        path.reset();
        path.moveTo((float) (corners[corners.length-1].x * ratio), (float) (corners[corners.length-1].y * ratio));
        for (Vector2 corner : corners) {
            path.lineTo((float) (corner.x * ratio),(float) (corner.y * ratio));
        }
        int offset = r.nextInt(48);
        paintCollection[0].setColor(Color.rgb(255 - 48 + offset, 0, 0));
        canvas.drawPath(path, paintCollection[0]);

        Paint glowing = new Paint();
        glowing.setColor(Color.WHITE);
        glowing.setShader(new LinearGradient(0,0,0,height, new int[] {Color.rgb(baseColorIntensity + offset - 24,0,0), Color.BLACK},null, Shader.TileMode.MIRROR));
        glowing.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));
        canvas.drawRect(0, (float)(corners[0].y - height) * (float)ratio, width * (float)ratio, (float)(corners[0].y)* (float)ratio, glowing);
        baseColorIntensity = 96;
    }
}
