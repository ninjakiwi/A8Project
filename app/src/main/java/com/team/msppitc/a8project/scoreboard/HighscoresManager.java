package com.team.msppitc.a8project.scoreboard;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class HighscoresManager implements Runnable {

    private boolean addNewScore;

    private String name;
    private int newScore;

    private ConnectionDataListener listener; // listener field

    public HighscoresManager(ConnectionDataListener listener) {
        addNewScore = false;
        this.listener = listener;
    }
    public HighscoresManager(int newScore, String name, ConnectionDataListener listener) {
        addNewScore = true;
        this.newScore = newScore;
        this.name = name;
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            ArrayList<String> allNames = new ArrayList<>();
            ArrayList<Integer> allScores = new ArrayList<>();
            String link;
            if (addNewScore) {
                link = "http://dreamlo.com/lb/F3dq9GPTOkaHdLynnRKBuQr8iGYjrfcUit8w3HOxHlMQ/add-pipe/" + name + "/" + newScore;
            } else {
                link = "http://dreamlo.com/lb/5bb7eee6613a87132cad3300/pipe";
            }
            URL url = new URL(link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            BufferedReader buf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            for (String line = buf.readLine(); line != null; line = buf.readLine()) {
                String[] splited = line.split("\\|");
                allNames.add(splited[0]);
                allScores.add(Integer.parseInt(splited[1]));
            }
            conn.disconnect();
            listener.dataReturned(allNames, allScores);

//            Log.d("202", "Response code: " + conn.getResponseCode());

        } catch (MalformedURLException e) {
            Log.e("202","Malformed URL");
        } catch (IOException e) {
            Log.e("202", "IO Error: " + e.getMessage());
//            listener.error();
        }
    }

}
