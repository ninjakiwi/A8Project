package com.team.msppitc.a8project.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.team.msppitc.a8project.R;
import com.team.msppitc.a8project.scoreboard.Scoreboard;
import com.team.msppitc.a8project.game.Playground;

public class HomeActivity extends Activity {

    public String username = "";
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        EditText nameField = findViewById(R.id.nameVal);
        View playButton = findViewById(R.id.play_button);
        playButton.setEnabled(false);

        nameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                username = charSequence.toString();
                if (username.replaceAll("[^a-zA-Z\\d ]", "").trim().length() >= 3) {
                    playButton.setEnabled(true);
                } else {
                    playButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void triggerPlay(View view) {
        Intent startGame = new Intent(HomeActivity.this, Playground.class);
        startActivityForResult(startGame, 1);
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        // Collect data from the intent and use it
        int score = data.getIntExtra("score", 0);
        if (score > 0) {
            score(score);
        }
    }

    public void triggerScores(View view) {
        Intent startScoreboard = new Intent(HomeActivity.this, Scoreboard.class);
        startActivity(startScoreboard);
    }

    public void score(int score) {
        Intent startScoreboard = new Intent(HomeActivity.this, Scoreboard.class);
        startScoreboard.putExtra("score", score);
        startScoreboard.putExtra("name", username);
        startActivity(startScoreboard);


    }
}
