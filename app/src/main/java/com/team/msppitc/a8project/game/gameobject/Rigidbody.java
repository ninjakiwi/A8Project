package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.team.msppitc.a8project.game.helpers.ConstraintManager;
import com.team.msppitc.a8project.game.helpers.Vector2;

public abstract class Rigidbody extends GameObject {

    private Vector2 velocity;
    private Vector2 acceleration;

    private Vector2 oldPos;

    private double angularVelocity; // Radians per second
    private double angularAcceleration;
    private double mass = 1;
    private double inertia;
    public double restitution = 0.5;

    private Paint constraintPaint;
    private static final int constraintColour = Color.DKGRAY;

    private ConstraintManager cm;

    private void init(double mass) {
        velocity = new Vector2();
        acceleration = new Vector2();
        this.mass = mass;
        oldPos = new Vector2(0,0);
        inertia = mass * (width* width + height * height) / 12; // Assume everything is a rectangle
        constraintPaint = new Paint();
        constraintPaint.setColor(constraintColour);
        constraintPaint.setStrokeWidth(10);
        constraintPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    /**
     * Deep clone
     */
    public Rigidbody(Rigidbody rb) {
        super(rb.position, rb.angle, rb.width, rb.height, rb.intendedColor);
        mass = rb.mass;
        restitution = rb.restitution;
        velocity = new Vector2(rb.velocity);
        acceleration = new Vector2(rb.acceleration);
        angularVelocity = rb.angularAcceleration;
        angularAcceleration = rb.angularAcceleration;
        oldPos = rb.oldPos;

        //FIXME: might be an issue if startingPos in cm is changed for some reason (shouldn't happen)
        cm = rb.cm;
    }

    public Rigidbody(int width, int height, int intendedColor, double mass) {
        super(width, height, intendedColor);
        init(mass);
    }

    public Rigidbody(Vector2 pos, double angle,int width, int height, int intendedColor, double mass) {
        super(pos, angle, width, height, intendedColor);
        init(mass);
    }

    public Rigidbody(Vector2 pos, double angle,int width, int height, int intendedColor, double mass, ConstraintManager cm) {
        super(pos, angle, width, height, intendedColor);
        this.cm = cm;
        init(mass);
    }

    public void Update(double deltaTime) {
//      double deltaTime = lastTime == 0 ? 1 / 60.0 : System.nanoTime();

        velocity.addMut(acceleration.mult(deltaTime));
        angularVelocity += angularAcceleration * deltaTime;

        acceleration.reset();
        angularAcceleration = 0;

        if (cm != null) {
            cm.constraintVelocity(deltaTime, this);
        }

        velocity.addMut(acceleration.mult(deltaTime));
        angularVelocity += angularAcceleration * deltaTime;
        oldPos.x = position.x;
        oldPos.y = position.y;

        angle += angularVelocity * deltaTime;
        position.addMut(velocity.mult(deltaTime));

        if (cm != null) {
            cm.constraintPosition(deltaTime, this);
        }

        velocity.multMut(0.995);
        angularVelocity *= 0.995;

        acceleration.reset();
        angularAcceleration = 0;
    }

    //protected void constraint(double deltaTime) {}

    public void addForce(Vector2 force) {
        acceleration.addMut(new Vector2(force.x / mass, force.y / mass));
    }

    /**
     * Adds force to the object at a particular point, to add impulse multiply by 1 / timestep
     * @param force The force to be added
     * @param position The point to add the force
     */
    public void addForce(Vector2 force, Vector2 position) {
        Vector2 positionDelta = position.add(this.position.mult(-1));
        double torque = positionDelta.x * force.y - positionDelta.y * force.x;
        angularAcceleration += torque / inertia;
        acceleration.addMut(new Vector2(force.x / mass, force.y / mass));
    }

    public Vector2 getVelocity(Vector2 position) {
        Vector2 positionDelta = position.add(this.position.mult(-1));
        double pointVelMag = angularVelocity * positionDelta.magnitude();
        return (positionDelta.equals(new Vector2()) ? new Vector2() : positionDelta.normalize().tangent().mult(pointVelMag)).add(this.velocity);
    }

    public Vector2 getVelocity() {
        return velocity;
    }
    public void setVelocity(Vector2 vel) {
        this.velocity = vel;
    }

    public double getMass() {
        return mass;
    }

    public void resetToLastPos() {
        position.x = oldPos.x;
        position.y = oldPos.y;
    }

    public double getAngle() {return angle;}

    public double getRestitution() {
        return restitution;
    }

    public Vector2 getMomentum() {
        return velocity.mult(mass);
    }

    public void setAngularVelocity(double angularVelocity) {
        this.angularVelocity = angularVelocity;
    }

    /**
     * Gets the angular velocity in radians per second
     */
    public double getAngularVelocity() {
        return angularVelocity;
    }

    public void setConstraintManager(ConstraintManager cm) {
        this.cm = cm;
    }

    public boolean hasConstraintManager() {return cm != null;}

    public void constraintManagerDrawRail(Canvas c, double s) {if (hasConstraintManager())cm.drawRails(c,s,constraintPaint);}
    public void constraintManagerDrawCenter(Canvas c, double s) {if (hasConstraintManager())c.drawCircle((float)(position.x*s),(float)(position.y*s),(float)(8*s),constraintPaint);}
}
