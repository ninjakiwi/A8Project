package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;

import com.team.msppitc.a8project.game.helpers.Vector2;

public class Player extends Circle {
    public Player(Vector2 pos, double angle, int radius, int intendedColor, double mass) {
        super(pos, angle, radius, intendedColor, mass);
        restitution = 2.5;
    }

    @Override
    public void Draw(Canvas canvas, double ratio) {
        super.Draw(canvas, ratio);
    }
}
