package com.team.msppitc.a8project.game.helpers;

import com.team.msppitc.a8project.game.gameobject.Circle;
import com.team.msppitc.a8project.game.gameobject.Rectangle;

public interface Collision { // Returns Hit pos and
    Vector2[] intersect(Collision collision);
    Vector2[] intersectCircle(Circle c);
    Vector2[] intersectRectangle(Rectangle r);
    double getBoundingRadius();
    Vector2 getPosition();
}
