package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Path;

import com.team.msppitc.a8project.game.helpers.Colliders;
import com.team.msppitc.a8project.game.helpers.Collision;
import com.team.msppitc.a8project.game.helpers.ConstraintManager;
import com.team.msppitc.a8project.game.helpers.Vector2;

import java.util.ArrayList;

public class Rectangle extends Rigidbody implements Collision {

    private double boundingRadius;

    public Rectangle(Vector2 pos, double angle, int width, int height, int intendedColor, double mass, ConstraintManager cm) {
        super(pos, angle, width, height, intendedColor, mass, cm);
        restitution = 0.5;
        boundingRadius = Math.sqrt(Math.pow(width/2.0,2) + Math.pow(height/2.0,2));
    }

    @Override
    public void Draw(Canvas canvas, double ratio) {

        Vector2[] corners = getCorners();

        Path path = new Path();
        path.reset();
        path.moveTo((float) (corners[corners.length-1].x * ratio), (float) (corners[corners.length-1].y * ratio));

        for (Vector2 corner : corners) {
            path.lineTo((float) (corner.x * ratio),(float) (corner.y * ratio));
        }

        canvas.drawPath(path, paintCollection[0]);

        constraintManagerDrawCenter(canvas, ratio);
    }

    public Vector2[] getCorners() {
        return new Vector2[] {
                new Vector2(position.x - width/2,position.y - height/2).rotateAround(angle, position),
                new Vector2(position.x + width/2,position.y - height/2).rotateAround(angle, position),
                new Vector2(position.x + width/2,position.y + height/2).rotateAround(angle, position),
                new Vector2(position.x - width/2,position.y + height/2).rotateAround(angle, position),
        };
    }

    @Override
    public Vector2[] intersect(Collision collision) {
        if (position.sub(collision.getPosition()).magnitudeSqr() < Math.pow(collision.getBoundingRadius() + getBoundingRadius(),2)) {
            return collision.intersectRectangle(this);
        } else {
            return null;
        }
    }

    @Override
    public Vector2[] intersectCircle(Circle c) {
        // Thing is a rectangle
        Vector2 circlePos = c.getPosition();
        double circleRadius = c.getRadius();
        Vector2[] recCorners = getCorners();
        double recAngle = getAngle();

        for (int i = 0; i < recCorners.length; i++) {
            // Check edge
            Vector2 edge = Colliders.intersectCircleOnLine(circlePos, circleRadius, recCorners[i], recCorners[(i+1) % recCorners.length], recAngle);
            if (edge != null) {
                Vector2 circleToEdge = circlePos.sub(edge);
                Vector2 directionOfImpact = circleToEdge.normalize();
                Vector2 pointOfFurtherestDecent = directionOfImpact.mult(c.getRadius()); // from circle
                return new Vector2[] {circlePos.sub(pointOfFurtherestDecent), pointOfFurtherestDecent.sub(circleToEdge)};
            }

            // Check corner
            if (circlePos.sub(recCorners[i]).magnitude() < circleRadius) {
                Vector2 circleToCorner = circlePos.sub(recCorners[i]);
                Vector2 directionOfImpact = circleToCorner.normalize();
                Vector2 pointOfFurtherestDecent = directionOfImpact.mult(c.getRadius()); // from circle
                return new Vector2[] {circlePos.sub(pointOfFurtherestDecent), pointOfFurtherestDecent.sub(circleToCorner)};
            }
        }
        return null;
    }

    @Override
    public Vector2[] intersectRectangle(Rectangle r) {
        // Thing is a rectangle
        Vector2[] cornersA = getCorners();
        Vector2[] cornersB = r.getCorners();

        ArrayList<Vector2> hitPoints = new ArrayList<>();

        Vector2 totalPoints = new Vector2();

        int[] cornersAHit = new int[] {0,0,0,0};
        int[] cornersBHit = new int[] {0,0,0,0};

        Edges[] edgesA = new Edges[] {
                new Edges(0,1),
                new Edges(1,2),
                new Edges(2,3),
                new Edges(3,0)
        };
        Edges[] edgesB = new Edges[] {
                new Edges(0,1),
                new Edges(1,2),
                new Edges(2,3),
                new Edges(3,0)
        };

        int amount = 0;

        for (Edges edge1 : edgesA) {
            for (Edges edge2 : edgesB) {
                Vector2 candi = Colliders.intersectLineWithLine(cornersA[edge1.cornerA], cornersA[edge1.cornerB],
                        cornersB[edge2.cornerA], cornersB[edge2.cornerB]);
                if (candi != null) {
                    amount++;
                    totalPoints.addMut(candi);
                    hitPoints.add(candi);
                    edge1.hit = 1;
                    edge2.hit = 1;
                }
            }
        }

        if (amount == 0) {
            return null;
        }

        // Now find the corner inside the other
        int box = 0; // Multiplier for delta, decides which way 1st box will move
        Vector2 corner = null;
        for (Edges edge1 : edgesA) {
            cornersAHit[edge1.cornerA] += edge1.hit;
            if (cornersAHit[edge1.cornerA] == 2) {
                corner = cornersA[edge1.cornerA];
                break;
            }
            cornersAHit[edge1.cornerB] += edge1.hit;
            if (cornersAHit[edge1.cornerB] == 2) {
                corner = cornersA[edge1.cornerB];
                break;
            }
        }
        if (corner == null) {
            box = 1;
            for (Edges edge2 : edgesB) {
                cornersBHit[edge2.cornerA] += edge2.hit;
                if (cornersBHit[edge2.cornerA] == 2) {
                    corner = cornersB[edge2.cornerA];
                    break;
                }
                cornersBHit[edge2.cornerB] += edge2.hit;
                if (cornersBHit[edge2.cornerB] == 2) {
                    corner = cornersB[edge2.cornerB];
                    break;
                }
            }
        } else { // Check if it's a corner cross
            for (Edges edge2 : edgesB) {
                cornersBHit[edge2.cornerA] += edge2.hit;
                cornersBHit[edge2.cornerB] += edge2.hit;
                if (cornersBHit[edge2.cornerA] == 2 || cornersBHit[edge2.cornerB] == 2) {
                    box = 2;
                    break;
                }
            }
        }
        if (corner == null) return null; //TODO: Box has 2 or no points in the square (forget about case for now)

        Vector2 hitPos; // hit pos is the point on the flat surface of a square (or in middle of corner cross)
        if (amount == 2) { // Should get the point of corner projected onto these lines
            hitPos = corner.add(hitPoints.get(0).mult(-1)).projection(hitPoints.get(1).add(hitPoints.get(0).mult(-1))).add(hitPoints.get(0));
        } else {
            //TODO: add(or subtract???) dist Across diff to this and multiply delta by two
            hitPos = totalPoints.mult(1 / (float) amount); // Average of all intersects, most cases will be on a line
        }

        Vector2 distAcrossDiff = corner.sub(hitPos);

        if (box == 0) {// corner from A is poking in B
            return new Vector2[]{hitPos, distAcrossDiff};
        } else if (box == 1) { // corner from B is poking into A
            return new Vector2[]{corner, distAcrossDiff.mult(-1)};
        } else { // corner from B is poking into A (<given corner) but also other way around
            return new Vector2[]{corner, distAcrossDiff.mult(2)}; //FIXME: I'm wrong
        }
    }

    @Override
    public double getBoundingRadius() {
        return boundingRadius;
    }

    private class Edges {
        int cornerA, cornerB;
        short hit;
        Edges(int _cornerA, int _cornerB) {
            cornerA = _cornerA;
            cornerB = _cornerB;
            hit = 0;
        }
    }
}
