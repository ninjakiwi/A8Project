package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;

import com.team.msppitc.a8project.game.helpers.ConstraintManager;
import com.team.msppitc.a8project.game.GameView;
import com.team.msppitc.a8project.game.helpers.ParticleSystem;
import com.team.msppitc.a8project.game.helpers.Vector2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class PointsCircle extends Circle {

    private int originalColor;
    private GameView gameView;
    private static Random r = new Random();

    public PointsCircle(GameView gameView, Vector2 pos, double angle, int radius, int intendedColor, double mass, ConstraintManager cm) {
        super(pos, angle, radius, intendedColor, mass, cm);
        originalColor = intendedColor;
        this.gameView = gameView;
        restitution = 5;
    }

    @Override
    public Vector2[] intersectCircle(Circle c) {
        Vector2[] hitPoints = super.intersectCircle(c);

        if (hitPoints != null && c instanceof Player) {
            intendedColor = BrightenColor(3, originalColor);
            Log.d("202", "HIT REGISTERED");
            gameView.score += 200;
            LinkedList<Rigidbody> stuff = gameView.getNonColliderRigidbodies();
            for (int i = 0; i < 10; i++) {
                stuff.add(new Particle(new Vector2(position), 5, Color.GREEN, 1, ParticleSystem.randomVecSpread(new Vector2(0,-1), 600 + getVelocity().y, Math.PI / 1.7, 250), 1 + r.nextDouble() * 0.5));
            }

        }
        return hitPoints;
    }

    @Override
    public void Draw(Canvas canvas, double ratio) {
        super.Draw(canvas, ratio);
        intendedColor = originalColor;
    }
}
