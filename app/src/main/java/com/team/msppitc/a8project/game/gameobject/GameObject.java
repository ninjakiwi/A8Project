package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Shader;

import com.team.msppitc.a8project.game.helpers.Vector2;

public abstract class GameObject {
    protected Vector2 position;
    protected double angle;
    protected int width,height;
    protected int intendedColor;

    protected Paint[] paintCollection;

    private boolean dead = false;

    public GameObject(int width, int height, int intendedColor) {
        this.width = width;
        this.height = height;
        this.intendedColor = intendedColor;
        angle = 0;
        position = new Vector2();
        paintCollection = MakeItGlow(intendedColor,1, position);
    }
    public GameObject(Vector2 pos, double angle, int width, int height, int intendedColor) {
        this.width = width;
        this.height = height;
        this.intendedColor = intendedColor;
        position = pos;
        this.angle = angle;
        paintCollection = MakeItGlow(intendedColor,1, pos);
    }

    public void move(Vector2 vector2) {
        position = position.add(vector2);
    }

    public abstract void Draw(Canvas canvas, double gameScaleRatio);

    public Vector2 getPosition() {
        return new Vector2(position);
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public static int BrightenColor(double amount, int clr) {
        float[] hsv = new float[3];
        Color.colorToHSV(clr, hsv);
        hsv[2] *= amount;
        hsv[1] /= amount;
        return Color.HSVToColor(hsv);
    }

    public static Paint[] MakeItGlow(int intendedColor, float scale, Vector2 position) {

        if (intendedColor == 0) {
            return new Paint[] {new Paint(), new Paint()};
        }

        Paint roleModel = new Paint();
        roleModel.setColor(intendedColor);
        roleModel.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));

        Paint other1 = new Paint();
        other1.setColor(intendedColor);

        roleModel.setShader(new RadialGradient((float)position.x,(float)position.y, scale, roleModel.getColor(), Color.BLACK, Shader.TileMode.CLAMP));
        //roleModel.setMaskFilter(new BlurMaskFilter(70 * scale, BlurMaskFilter.Blur.OUTER));

        return new Paint[] {other1, roleModel};
    }

    public static double lerp(double a, double b, double f)
    {
        return (a * (1.0 - f)) + (b * f);
    }

    public boolean isDead() {
        return dead;
    }
    public void Destroy() {
        dead = true;
    }
}
