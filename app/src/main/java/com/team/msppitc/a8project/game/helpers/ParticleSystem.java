package com.team.msppitc.a8project.game.helpers;

import com.team.msppitc.a8project.game.gameobject.Particle;
import com.team.msppitc.a8project.game.gameobject.Rigidbody;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class ParticleSystem {
//    private Particle particleToSpawn;
    private Vector2 position;
    private Vector2 directionToSpawn;
    private double force;
    private double cooldown;
    private double currentCooldown;

    private int color;

    static Random r = new Random();

    public ParticleSystem(Vector2 position, Vector2 directionToSpawn, double force, double spawnRate, int intendedParticleColor) {
        this.position = position;
        this.directionToSpawn = directionToSpawn;
        this.force = force;
        this.cooldown = spawnRate;
        currentCooldown = cooldown;
        color = intendedParticleColor;
    }

    public void Update(double delta, LinkedList<Rigidbody> allBodies) {
        currentCooldown -= delta;

        if (currentCooldown < 0) {
            currentCooldown += cooldown *(r.nextDouble() * 0.5 + 0.75);
            Particle particle = new Particle(new Vector2(position), 5, color, 1, randomVecSpread(directionToSpawn, force, 0.8, force * 0.5), 2 + r.nextDouble() * 1.5);
            allBodies.add(particle);
        }
    }

    public static Vector2 randomVecSpread(Vector2 initDir, double baseForce, double maxRandomSpreadAmount, double maxRandomForce) {
        return initDir.rotate(r.nextDouble() * maxRandomSpreadAmount - maxRandomSpreadAmount * 0.5)
                .mult(baseForce + (r.nextDouble() * maxRandomForce - maxRandomForce * 0.5));
    }
}
