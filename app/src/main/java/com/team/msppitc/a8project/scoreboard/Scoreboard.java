package com.team.msppitc.a8project.scoreboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.team.msppitc.a8project.R;

public class Scoreboard extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        boolean connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
        TextView conInfoText = findViewById(R.id.connectionInfo);
        if (connected) {
            conInfoText.setText(R.string.con_info_connected);
        } else {
            conInfoText.setText(R.string.con_info_failed);
        }

        if (!connected) {
            return;
        }


        Intent data = getIntent();

        String user = data.getStringExtra("name");

        int userScore = data.getIntExtra("score", 0);

        Thread hs;
            if (user != null) {
                final String user2 = user.replaceAll("[^a-zA-Z\\d ]", "").trim();
                TextView currentScore = findViewById(R.id.currentScore);
                currentScore.setText(String.valueOf(userScore));

                hs = new Thread(new HighscoresManager(userScore, user2.replaceAll("[ ]", "%20"), (allNames, allScores) -> runOnUiThread(() -> {
                    conInfoText.setText(R.string.con_info_loaded);
                    TableLayout table = findViewById(R.id.boardTable);
                    for (int i = 0; i < allNames.size(); i++) {
                        TableRow row = new TableRow(this);
                        TextView name = new TextView(this);
                        TextView score = new TextView(this);
                        TextView place = new TextView(this);
                        place.setText(String.format("%s. ", String.valueOf(i + 1)));
                        name.setText(allNames.get(i));
                        score.setText(String.valueOf(allScores.get(i)));
                        score.setGravity(Gravity.RIGHT);

                        score.setTextSize(20);
                        name.setTextSize(20);
                        place.setTextSize(20);

                        if (allNames.get(i).equals(user2) && allScores.get(i) == userScore) {
                            score.setTextColor(Color.rgb(255, 128, 0));
                            name.setTextColor(Color.rgb(255, 128, 0));
                            place.setTextColor(Color.rgb(255, 128, 0));
                        }

                        row.addView(place);
                        row.addView(name);
                        row.addView(score);
                        table.addView(row);
                    }

                    ProgressBar pb = findViewById(R.id.progressBar);
                    pb.setVisibility(View.INVISIBLE);
                })));

            } else {
                hs = new Thread(new HighscoresManager((allNames, allScores) -> runOnUiThread(() -> {

                    conInfoText.setText(R.string.con_info_loaded);
                    TableLayout table = findViewById(R.id.boardTable);
                    for (int i = 0; i < allNames.size(); i++) {
                        TableRow row = new TableRow(this);
                        TextView name = new TextView(this);
                        TextView score = new TextView(this);
                        TextView place = new TextView(this);
                        place.setText(String.format("%s. ", String.valueOf(i + 1)));
                        name.setText(allNames.get(i));
                        score.setText(String.valueOf(allScores.get(i)));
                        score.setGravity(Gravity.RIGHT);

                        score.setTextSize(20);
                        name.setTextSize(20);
                        place.setTextSize(20);

                        row.addView(place);
                        row.addView(name);
                        row.addView(score);
                        table.addView(row);
                    }

                    ProgressBar pb = findViewById(R.id.progressBar);
                    pb.setVisibility(View.INVISIBLE);
                })));
            }

            hs.start();

    }
}
