package com.team.msppitc.a8project.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import com.team.msppitc.a8project.game.helpers.Vector2;

public class Particle extends Rigidbody {

    private static final double GRAVITY = 98 * 5;

    private double lifetime;
    private double lifetimeLive;

    public Particle(Vector2 pos, double radius, int intendedColor, double mass, Vector2 initialVelocity, double lifetime) {
        super(pos, 0, (int)(radius * 2), (int)(radius * 2), intendedColor, mass);
        this.lifetime = lifetime;
        this.lifetimeLive = lifetime;
        setVelocity(initialVelocity);
    }

    @Override
    public void Update(double deltaTime) {
        addForce(new Vector2(0, GRAVITY * getMass()));
        lifetimeLive -= deltaTime;

        super.Update(deltaTime);

        if (getPosition().y > 2000 || lifetimeLive < 0) {
            Destroy();
        }
    }

    @Override
    public void Draw(Canvas canvas, double ratio) {
        Paint[] allPaints = MakeItGlow(BrightenColor(lerp(0, 1, (Math.min(lifetimeLive, lifetime * 0.5)/(lifetime * 0.5))),intendedColor), (float)(getRadius() * 2.5 * ratio), position.mult(ratio));
        for (int i = 1; i < allPaints.length; i++) {
            canvas.drawCircle((float) (position.x * ratio), (float) (position.y * ratio), (float)(getRadius() * ratio * 2.5), allPaints[i]);
        }
        allPaints[0].setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));
        canvas.drawCircle((float) (position.x * ratio), (float) (position.y * ratio), (float)(getRadius() * ratio), allPaints[0]);
    }

    public double getRadius() {
        return width/2.0;
    }
}
