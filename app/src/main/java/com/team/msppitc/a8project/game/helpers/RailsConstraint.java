package com.team.msppitc.a8project.game.helpers;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.team.msppitc.a8project.game.gameobject.Rigidbody;

public class RailsConstraint extends ConstraintManager {

    private boolean rotatable = false;
    private double length;
    private Vector2 dir;
    private Vector2 lastPosition;


    public RailsConstraint(Vector2 startingPos,boolean rotatable, double length, Vector2 dir) {
        super(startingPos);
        this.rotatable = rotatable;
        this.length = length;
        this.dir = dir;
        lastPosition = startingPos;
    }

    @Override
    public void constraintVelocity(double deltaTime, Rigidbody rb) {
        Vector2 temp = rb.getVelocity().projection(dir);
        rb.setVelocity(temp);

        if (!rotatable) {
            rb.setAngularVelocity(0);
        }
    }

    public boolean isRotatable() {
        return rotatable;
    }

    public void setRotatable(boolean rotatable) {
        this.rotatable = rotatable;
    }

    @Override
    public void constraintPosition(double deltaTime, Rigidbody rb) {
        double lengthTemp = rb.getPosition().sub(startingPos).magnitudeSqr();
        if (lengthTemp > length * length) {
            rb.setVelocity(rb.getVelocity().sub(rb.getPosition().sub(lastPosition).mult(1/deltaTime)));
            rb.setPosition(lastPosition);
        }
        lastPosition = new Vector2(rb.getPosition());
    }

    @Override
    public void drawRails(Canvas canvas, double scale, Paint paint) {
        Vector2 pos1 = startingPos.add(dir.mult(-length));
        Vector2 pos2 = startingPos.add(dir.mult(length));

        canvas.drawLine((float) (pos1.x*scale), (float) (pos1.y*scale), (float) (pos2.x*scale), (float) (pos2.y*scale), paint);
    }
}
