package com.team.msppitc.a8project.game.helpers;

public class Colliders {
    public static Vector2 intersectCircleOnLine(Vector2 C, double circleRadius, Vector2 A, Vector2 B,double angle) {
        Vector2 Ar = A.rotateAround(-angle,C);
        Vector2 Br = B.rotateAround(-angle,C);
        Vector2 newPos = new Vector2();

        if (Vector2.doubleEquals(Ar.x, Br.x)) {
            if (C.x - circleRadius >= Math.max(Ar.x, Br.x) || C.x + circleRadius <= Math.min(Ar.x, Br.x)) {
                return null;
            }
            if (C.y >= Math.max(Ar.y, Br.y) || C.y <= Math.min(Ar.y, Br.y)) {
                return null;
            }
            newPos.x = Ar.x;
            newPos.y = C.y;
        } else {
            if (C.x >= Math.max(Ar.x, Br.x) || C.x <= Math.min(Ar.x, Br.x)) {
                return null;
            }
            if (C.y - circleRadius >= Math.max(Ar.y, Br.y) || C.y + circleRadius <= Math.min(Ar.y, Br.y)) {
                return null;
            }
            newPos.x = C.x;
            newPos.y = Ar.y;
        }

        if (C.y - circleRadius >= Math.max(Ar.y, Br.y) || C.y + circleRadius <= Math.min(Ar.y, Br.y)) {
            return null;
        }
        return newPos.rotateAround(angle, C);
    }

    public static Vector2 intersectLineWithLine(Vector2 A, Vector2 B, Vector2 C, Vector2 D)
    {
        double CmPx = C.x - A.x;
        double CmPy = C.y - A.y;
        double rx = B.x - A.x;
        double ry = B.y - A.y;
        double sx = D.x - C.x;
        double sy = D.y - C.y;

        double CmPxr = CmPx * ry - CmPy * rx;
        double CmPxs = CmPx * sy - CmPy * sx;
        double rxs = rx * sy - ry * sx;

        if (CmPxr == 0f)
        {
            // Lines are collinear, and so intersect if they have any overlap

            return null;
        }

        if (rxs == 0f)
            return null; // Lines are parallel.

        double rxsr = 1f / rxs;
        double intersectMagicx = CmPxs * rxsr;
        double intersectMagicy = CmPxr * rxsr;

        if ((intersectMagicx >= 0f) && (intersectMagicx <= 1f) && (intersectMagicy >= 0f) && (intersectMagicy <= 1f)) {

            if (Vector2.doubleEquals(rx, 0.0)) {

                double m2 = sy/sx;
                double c2 = -m2 * C.x + C.y;

                return new Vector2(A.x, c2 + m2 * A.x);

            } else if (Vector2.doubleEquals(sx, 0.0)) {

                double m1 = ry/rx;
                double c1 = -m1 * A.x + A.y;

                return new Vector2(C.x, c1 + m1 * C.x);

            } else {

                double m1 = ry/rx;
                double c1 = -m1 * A.x + A.y;
                double m2 = sy/sx;
                double c2 = -m2 * C.x + C.y;

                double x = (c1-c2)/(m2-m1);
                return new Vector2(x, c2 + m2 * x);

            }

        } else {
            return null;
        }
    }
}
