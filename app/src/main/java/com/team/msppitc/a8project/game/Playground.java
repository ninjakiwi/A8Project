package com.team.msppitc.a8project.game;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.TextView;

import com.team.msppitc.a8project.R;
import com.team.msppitc.a8project.game.helpers.GameListener;

public class Playground extends Activity implements SurfaceHolder.Callback {

    public GameView mGameView;
    public TextView mScoreText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playground);

        mGameView = findViewById(R.id.playView);
        mScoreText = findViewById(R.id.score);
        mScoreText.setText("Score: 0");

        GameListener gameListener = new GameListener() {
            @Override
            public void endGame(int score) {
                Intent intent = new Intent();
                intent.putExtra("score", score);
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void updateScore(int score) {
                runOnUiThread(() -> mScoreText.setText("Score: " + score));
            }
        };
        mGameView.setGameListener(gameListener);


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mGameView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("score", 0);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGameView.resume();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("danielDebug", "surfaceDestroyed: noooooooo");
    }

}
