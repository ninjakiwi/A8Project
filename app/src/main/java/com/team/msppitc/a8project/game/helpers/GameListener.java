package com.team.msppitc.a8project.game.helpers;

public interface GameListener {
    void endGame(int score);
    void updateScore(int score);
}
