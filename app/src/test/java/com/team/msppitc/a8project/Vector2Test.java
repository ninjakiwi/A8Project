package com.team.msppitc.a8project;

import com.team.msppitc.a8project.game.helpers.Vector2;

import org.junit.Test;

import static org.junit.Assert.*;

public class Vector2Test {
    @Test
    public void addition_isCorrect() {
        assertEquals(new Vector2(5, 7).add(new Vector2(12.22, 4)), new Vector2(17.22, 11));
        assertEquals(new Vector2(5, 7.1).add(new Vector2(12, 4.33)), new Vector2(17, 11.43));
        assertEquals(new Vector2(-5, 7).add(new Vector2(12, 4.33)), new Vector2(7, 11.33));
    }

    @Test
    public void scalarMulti_isCorrect() {
        assertEquals(new Vector2(5, 7.3).mult(5), new Vector2(25, 36.5));
        assertEquals(new Vector2(-5, 7.3).mult(-5), new Vector2(25, -36.5));
        assertEquals(new Vector2(5, 7.3).mult(0.775), new Vector2(3.875, 5.6575));
        assertEquals(new Vector2(5, 7.3).mult(-0.775), new Vector2(-3.875, -5.6575));
        assertEquals(new Vector2(5, -7.3).mult(-0.775), new Vector2(-3.875, 5.6575));
    }

    @Test
    public void normalize_isCorrect() {
        assertEquals(new Vector2(1, 0).normalize(), new Vector2(1, 0));
        assertEquals(new Vector2(8, 0).normalize(), new Vector2(1, 0));
        assertEquals(new Vector2(0, 8).normalize(), new Vector2(0, 1));
        assertEquals(new Vector2(2, 8).normalize(), new Vector2(0.24253562503633297351890, 0.97014250014533189407562));
    }

    @Test
    public void magnitude_isCorrect() {
        assertTrue(Vector2.doubleEquals(new Vector2(0, 5).magnitude(), 5));
        assertTrue(Vector2.doubleEquals(new Vector2(5, 0).magnitude(), 5));
        assertTrue(Vector2.doubleEquals(new Vector2(8, 2).magnitude(), 8.2462112512353210996428));
        assertTrue(Vector2.doubleEquals(new Vector2(-8, 2).magnitude(), 8.2462112512353210996428));
        assertTrue(Vector2.doubleEquals(new Vector2(-8, -2).magnitude(), 8.2462112512353210996428));
    }

    @Test
    public void nearlyEqual_isCorrect() {
        assertTrue(Vector2.doubleEquals(1.213549543, 1.2135495431));
        assertFalse(Vector2.doubleEquals(1.213548, 1.213549));
    }

    @Test
    public void dotProduct_isCorrect() {
        assertTrue(Vector2.doubleEquals(new Vector2(1,2).dot(new Vector2(3, -8)), -13));
    }


    @Test
    public void projection_isCorrect() {
        assertEquals(new Vector2(1,2).projection(new Vector2(3, -8)), new Vector2(-39/73.0, 104/73.0));
        assertEquals(new Vector2(3, -8).projection(new Vector2(1, 2)), new Vector2(-13/5.0, -26/5.0));
    }
}
