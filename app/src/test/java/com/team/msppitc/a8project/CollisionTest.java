package com.team.msppitc.a8project;

import com.team.msppitc.a8project.game.gameobject.Circle;
import com.team.msppitc.a8project.game.helpers.Colliders;
import com.team.msppitc.a8project.game.gameobject.Rectangle;
import com.team.msppitc.a8project.game.helpers.Vector2;

import org.junit.Test;

import static org.junit.Assert.*;

public class CollisionTest {
    @Test
    public void circleCircleCollisionNoTouching_isCorrect() {
        Circle firstCircle = new Circle(new Vector2(10, 10), 0, 2, 0, 1);
        Circle secondCircle = new Circle(new Vector2(10, 14), 0, 2,0, 1);
        Vector2[] item = firstCircle.intersect(secondCircle);
        assertNull(item);
    }

    @Test
    public void circleCircleCollision_isCorrect() {
        Circle firstCircle = new Circle(new Vector2(10, 10), 0, 2,0, 1);
        Circle secondCircle = new Circle(new Vector2(10, 13), 0, 2, 0, 1);
        Vector2[] items = firstCircle.intersect(secondCircle);
        assertEquals(items[0], new Vector2(10,12));
        assertEquals(items[1], new Vector2(0,-1));
    }

    @Test
    public void rectRectCollision_isCorrect() {
        Rectangle firstRect = new Rectangle(new Vector2(10, 10), 0, 2,2, 0,1,  null);
        Rectangle secondRect = new Rectangle(new Vector2(12, 10), 0.01 , 2, 2, 0,1,null);

        Vector2[] hitData = firstRect.intersect(secondRect);

    }

    @Test
    public void lineLineIntersect_isCorrect() {
        Vector2 a1 = new Vector2 (0,0),b1 = new Vector2(1,0), a2 = new Vector2(2,2), b2 = new Vector2(1,2);
        Vector2 ret = Colliders.intersectLineWithLine(a1, a2, b1, b2);
        Vector2 re2 = Colliders.intersectLineWithLine(a2, a1, b1, b2);
        Vector2 re3 = Colliders.intersectLineWithLine(a2, a1, b2, b1);
        assertEquals(new Vector2(1,1), ret);
        assertEquals(new Vector2(1,1), re2);
        assertEquals(new Vector2(1,1), re3);
    }
}
